# Projeto campeonato

### URL do projeto no heroku

* https://vast-ocean-07609.herokuapp.com/

### Configurações para executar o projeto local

1. Docker do PHP configurado: ```sudo docker pull lhuggler/php-xdebug```
2. Mapear a porta com a pasta: ```sudo docker run --name php -p 80:80 -v /www:/www -td lhuggler/php-xdebug```
3. Docker do MySQL configurado: ```sudo docker pull mysql:5.6```
4. Mapear a porta e a senha do banco de dados: ```sudo docker run -d --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123 mysql:5.6```
5. Acessar a pasta: ```cd /www```
6. Dar permissão para a pasta: ```sudo chown nomedeusuario:nomedeusuario .```
7. Clonar o projeto: ```git clone https://marcoaurelio97@bitbucket.org/marcoaurelio97/ma_au.git```
8. Abrir um programa de gerenciamento de banco de dados e executar os scripts necessários (as configurações de conexão ao banco e os scripts estão no arquivo ```database.sql``` na raiz do projeto)
9. Executar o docker do PHP e MySQL: ```sudo docker start php mysql```
10. Acessar a URL ```http://localhost/ma_au/```

### Funções do projeto

* Ao criar um novo campeonato, os times serão gerados e organizados em grupos aleatóriamente
* Ao clicar em uma fase do campeonato, ela será processada e os resultados serão exibidos
* Os times classificados serão destacados em verde e os times que perderam em vermelho
* Para executar os testes unitários, acesse o endpoint ```tests```

### Arquitetura

* Frontend: Vue
* Backend: PHP
* Banco de dados: MySQL

### Observações

* O desenho da arquitetura e a modelagem de dados estão nos arquivos ```arquitetura.png``` e ```modelagem_dados.png``` na raiz do projeto