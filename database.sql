-- Hostname: 172.17.0.1
-- Port: 3306
-- Username: root
-- Password: 123

CREATE DATABASE IF NOT EXISTS `championship`;

CREATE TABLE IF NOT EXISTS `championship`.`teams` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(45) NOT NULL,
  `team_group` int(11) DEFAULT NULL,
  PRIMARY KEY (`team_id`)
);

CREATE TABLE IF NOT EXISTS `championship`.`playoffs` (
  `play_id` int(11) NOT NULL AUTO_INCREMENT,
  `play_team1` int(11) NOT NULL,
  `play_team2` int(11) NOT NULL,
  `play_type` varchar(45) NOT NULL,
  PRIMARY KEY (`play_id`),
  KEY `fk_playoffs_team1_idx` (`play_team1`),
  KEY `fk_playoffs_team2_idx` (`play_team2`),
  CONSTRAINT `fk_playoffs_team1` FOREIGN KEY (`play_team1`) REFERENCES `teams` (`team_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_playoffs_team2` FOREIGN KEY (`play_team2`) REFERENCES `teams` (`team_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS `championship`.`games` (
  `ga_id` int(11) NOT NULL AUTO_INCREMENT,
  `ga_team1` int(11) NOT NULL,
  `ga_team1_rounds` int(11) NOT NULL,
  `ga_team2` int(11) NOT NULL,
  `ga_team2_rounds` int(11) NOT NULL,
  `ga_winner` int(11) NOT NULL,
  `ga_type` varchar(45) NOT NULL,
  PRIMARY KEY (`ga_id`),
  KEY `fk_games_team1_idx` (`ga_team1`),
  KEY `fk_games_team2_idx` (`ga_team2`),
  KEY `fk_games_winner_idx` (`ga_winner`),
  CONSTRAINT `fk_games_team1` FOREIGN KEY (`ga_team1`) REFERENCES `teams` (`team_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_team2` FOREIGN KEY (`ga_team2`) REFERENCES `teams` (`team_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_winner` FOREIGN KEY (`ga_winner`) REFERENCES `teams` (`team_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
