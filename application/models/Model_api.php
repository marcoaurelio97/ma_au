<?php
class Model_api extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function generateChampionship()
    {
        $this->db->trans_begin();

        $this->clearDatabase();
        $this->addTeams();
        $this->addGroups();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            http_response_code(500);
            $status = false;
        } else {
            $this->db->trans_commit();
            http_response_code(200);
            $status = true;
        }

        return [ "status" => $status ];
    }

    private function clearDatabase()
    {
        $this->db->where("play_id >", "0");
        $this->db->delete("playoffs");
        
        $this->db->where("ga_id >", "0");
        $this->db->delete("games");

        $this->db->where("team_id >", "0");
        $this->db->delete("teams");
    }

    public function addTeams($qty = "80")
    {
        $db = [ "team_name" => "" ];

        for ($i = 1; $i <= $qty; $i++) {
            $db["team_name"] = "Time {$i}";
            $this->db->insert("teams", $db);
        }
    }

    public function getTeams($group = false)
    {
        if ($group) {
            $this->db->where("team_group", $group);
        }

        $teams = $this->db->get("teams");
        return ($teams && $teams->num_rows()) ? $teams->result() : [];
    }

    public function updateTeam($idTeam, $db)
    {
        $this->db->where("team_id", $idTeam);
        $this->db->update("teams", $db);
    }

    public function addGroups($qtyGroups = "16", $qtyTeamsGroup = "5")
    {
        $teams = $this->getTeams();

        for ($i = 1; $i <= $qtyGroups; $i++) {
            $random = array_rand($teams, $qtyTeamsGroup);

            foreach ($random AS $rd) {
                $this->updateTeam($teams[$rd]->team_id, ["team_group" => $i]);
                unset($teams[$rd]);
            }
        }
    }

    public function generateGames($type)
    {
        if ($this->verifyPhaseExists($type)) {
            return [ "status" => true ];
        }

        switch ($type) {
            case 'group-stage':
                return $this->groupStage();
                break;
            case 'first-phase':
                return $this->playoffs($type, "octaves", "8");
                break;
            case 'octaves':
                return $this->playoffs($type, "quarters", "4");
                break;
            case 'quarters':
                return $this->playoffs($type, "semifinals", "2");
                break;
            case 'semifinals':
                return $this->playoffs($type, "finals", "1");
                break;
            case 'finals':
                return $this->playoffs($type, "winner", "0");
                break;
            default:
                return [ "status" => false ];
                break;
        }
    }

    private function groupStage()
    {
        $this->db->trans_begin();

        for ($group = 1; $group <= 16; $group++) {
            $teams = $this->getTeams($group);
            $matches = [];

            foreach ($teams AS $team1) {
                foreach ($teams AS $team2) {
                    $team1ID = $team1->team_id;
                    $team2ID = $team2->team_id;

                    if ($team1ID != $team2ID && (!isset($matches["{$team1ID}-{$team2ID}"]) && !isset($matches["{$team2ID}-{$team1ID}"]))) {
                        $matches["{$team1ID}-{$team2ID}"] = $this->simulateGame($team1ID, $team2ID, "group-stage");
                    }
                }
            }

            $this->saveGames($matches);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            http_response_code(500);
            $status = false;
        } else {
            $this->db->trans_commit();
            $this->classifiedTeams(16);

            http_response_code(200);
            $status = true;
        }

        return [ "status" => $status ];
    }

    private function playoffs($type, $nextPhase, $qtyNextPhase)
    {
        $this->db->trans_begin();

        $firstPhase = $this->getTeamsPlayoffs($type);

        if (!$firstPhase) {
            return [ "status" => false ];
        }

        $matches = [];

        foreach ($firstPhase AS $teams) {
            $matches[] = $this->simulateGame($teams->play_team1, $teams->play_team2, $type);
        }

        $this->saveGames($matches);

        if ($nextPhase != "winner") {
            $this->classifyNextPhase($matches, $qtyNextPhase, $nextPhase);
        } else {
            $winner = [[
                "play_team1" => $matches[0]["ga_winner"],
                "play_team2" => $matches[0]["ga_winner"],
                "play_type"  => "winner"
            ]];

            $this->savePlayoffs($winner);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            http_response_code(500);
            $status = false;
        } else {
            $this->db->trans_commit();
            http_response_code(200);
            $status = true;
        }

        return [ "status" => $status ];
    }

    public function simulateGame($team1ID, $team2ID, $type)
    {
        $match = [
            "ga_team1" => $team1ID,
            "ga_team1_rounds" => 0,
            "ga_team2" => $team2ID,
            "ga_team2_rounds" => 0,
            "ga_type" => $type
        ];

        while (true) {
            $score = rand(0, 1);

            if ($score) {
                $match["ga_team2_rounds"]++;
            } else {
                $match["ga_team1_rounds"]++;
            }

            if ($match["ga_team1_rounds"] == 16) {
                $match["ga_winner"] = $team1ID;
                break;
            } else if ($match["ga_team2_rounds"] == 16) {
                $match["ga_winner"] = $team2ID;
                break;
            }
        }

        return $match;
    }

    private function saveGames($games)
    {
        $this->db->insert_batch("games", $games);
    }

    private function savePlayoffs($playoffs)
    {
        $this->db->insert_batch("playoffs", $playoffs);
    }

    private function classifiedTeams($qtyGroup)
    {
        for ($group = 1; $group <= $qtyGroup; $group++) {
            $teams = $this->getTeamsGroupStage($group);

            $playoffs[] = [
                "play_team1" => $teams[0]->team_id,
                "play_team2" => $teams[1]->team_id,
                "play_type"  => "first-phase"
            ];
        }

        $this->savePlayoffs($playoffs);
    }

    public function getTeamsPhase($type)
    {
        if ($type == "group-stage") {
            $teams = $this->getTeamsGroupStage(false, $type);
        } else {
            return $this->getTeamsGroupPhase($type);
        }

        $phase = [];

        foreach ($teams AS $team) {
            $phase[$team->team_group][] = [
                "name" => $team->team_name,
                "wins" => $team->wins,
                "rounds" => $team->rounds
            ];
        }

        return $phase;
    }

    private function getTeamsGroupStage($group = false, $type = false)
    {
        $sql_group = ($group) ? "WHERE team_group = {$group}" : "";
        $sql_type  = ($type)  ? "ga_type = '{$type}' AND" : "";

        $teams = $this->db->query("
            SELECT 
                team_id,
                team_name,
                team_group,
                (SELECT 
                        COUNT(*)
                    FROM
                        games
                    WHERE
                        {$sql_type} ga_winner = team_id) wins,
                COALESCE((SELECT 
                                SUM(IF(ga_team1 = team_id,
                                        ga_team1_rounds - ga_team2_rounds,
                                        ga_team2_rounds - ga_team1_rounds))
                            FROM
                                games
                            WHERE
                                {$sql_type} (ga_team1 = team_id OR ga_team2 = team_id)),
                        0) rounds
            FROM
                teams
                {$sql_group}
            ORDER BY wins DESC , rounds DESC
        ");

        return ($teams && $teams->num_rows()) ? $teams->result() : [];
    }

    private function getTeamsGroupPhase($type)
    {
        $teams = $this->db->query("
            SELECT 
                t1.team_id team1_id,
                t1.team_name team1_name,
                ga_team1_rounds team1_rounds,
                t2.team_id team2_id,
                t2.team_name team2_name,
                ga_team2_rounds team2_rounds,
                ga_winner winner
            FROM
                games
                    JOIN
                teams t1 ON t1.team_id = ga_team1
                    JOIN
                teams t2 ON t2.team_id = ga_team2
            WHERE
                ga_type = '{$type}'
        ");

        return ($teams && $teams->num_rows()) ? $teams->result() : [];
    }

    private function getTeamsPlayoffs($type)
    {
        $this->db->where("play_type", $type);
        $teams = $this->db->get("playoffs");

        return ($teams && $teams->num_rows()) ? $teams->result() : [];
    }

    private function classifyNextPhase($matches, $qtyGroups, $nextPhase)
    {
        $playoffs = [];

        for ($i = 1; $i <= $qtyGroups; $i++) {
            $random = array_rand($matches, 2);

            $playoffs[] = [
                "play_team1" => $matches[$random[0]]["ga_winner"],
                "play_team2" => $matches[$random[1]]["ga_winner"],
                "play_type"  => $nextPhase
            ];

            unset($matches[$random[0]]);
            unset($matches[$random[1]]);
        }

        $this->savePlayoffs($playoffs);
    }

    public function verifyPhaseExists($type)
    {
        $this->db->where("ga_type", $type);
        $phase = $this->db->get("games");

        return ($phase && $phase->num_rows()) ? true : false;
    }

    public function verifyDatabase()
    {
        if (!$this->getTeams()) {
            $this->generateChampionship();
        }
    }
}