<?php $this->load->view("header"); ?>

<div id="app">
    <div class="row justify-content-center">
        <button v-if="!loading" type="button" class="btn btn-outline-info mb-2" v-on:click="newChampionship">Novo campeonato</button>
        <button v-if="!loading" type="button" :class="(type == 'group-stage' && !new_champ) ? 'active' : ''" class="btn btn-outline-primary ml-2 mb-2" v-on:click="generatePhase('group-stage')">Fase de grupos</button>
        <button v-if="!loading" type="button" :class="type == 'first-phase' ? 'active' : ''" class="btn btn-outline-primary ml-2 mb-2" v-on:click="generatePhase('first-phase')">Primeira fase</button>
        <button v-if="!loading" type="button" :class="type == 'octaves'     ? 'active' : ''" class="btn btn-outline-primary ml-2 mb-2" v-on:click="generatePhase('octaves')">Oitavas</button>
        <button v-if="!loading" type="button" :class="type == 'quarters'    ? 'active' : ''" class="btn btn-outline-primary ml-2 mb-2" v-on:click="generatePhase('quarters')">Quartas</button>
        <button v-if="!loading" type="button" :class="type == 'semifinals'  ? 'active' : ''" class="btn btn-outline-primary ml-2 mb-2" v-on:click="generatePhase('semifinals')">Semifinal</button>
        <button v-if="!loading" type="button" :class="type == 'finals'      ? 'active' : ''" class="btn btn-outline-primary ml-2 mb-2" v-on:click="generatePhase('finals')">Final</button>
    </div>
    <br>
    <div class="row justify-content-center" v-if="!loading">
        <template v-if="group_stage">
            <template v-for="(groups, index) in teams[0]">
                <div class="col-sm-6 col-md-4 col-xl-3 mb-5">
                    <div class="shadow-sm text-center border border-secondary">
                        <h5 class="mt-2">Grupo {{index}}</h5>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Time</th>
                                    <th>Vitórias</th>
                                    <th>Pontos</th>
                                </tr>
                            </thead>
                            <tbody>
                                <template v-for="(team, index2) in groups">
                                    <tr :class="!new_champ ? (index2 == 0 || index2 == 1 ? 'text-success' : 'text-danger') : ''">
                                        <td>{{team.name}}</td>
                                        <td>{{team.wins}}</td>
                                        <td>{{team.rounds}}</td>
                                    </tr>
                                </template>
                            </tbody>
                        </table>
                    </div>
                </div>
            </template>
        </template>
        <template v-else>
            <template v-for="team in teams">
                <div class="col-sm-6 col-md-4 col-xl-3 mb-5">
                    <div class="shadow-sm text-center border border-secondary">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Time</th>
                                    <th>Rounds</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr :class="!new_champ ? (team.team1_id == team.winner ? 'text-success' : 'text-danger') : ''">
                                    <td>{{team.team1_name}}</td>
                                    <td>{{team.team1_rounds}}</td>
                                </tr>
                                <tr :class="!new_champ ? (team.team2_id == team.winner ? 'text-success' : 'text-danger') : ''">
                                    <td>{{team.team2_name}}</td>
                                    <td>{{team.team2_rounds}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </template>
        </template>
    </div>
    <div class="row justify-content-center">
        <div class="col-4 text-center">
            <button v-if="loading" class="btn btn-outline-primary" type="button" disabled>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                Carregando
            </button>
            <div v-if="teams.length == 0 && !loading" class="card shadow-sm">
                <div class="card-body text-center">Não foi possível carregar as informações.</div>
            </div>
        </div>
    </div>
</div>

<script>
	var app = new Vue({
		el: "#app",
		data: {
			loading: false,
            group_stage: true,
            teams: [],
            type: "group-stage",
            new_champ: true
		},
		methods : {
			newChampionship: function() {
                if (confirm("Deseja realmente gerar um novo campeonato?")) {
                    let url = base_url + 'api/championship';
                    this.loading = true;
                    this.new_champ = true;
                    this.type = "group-stage";
                    this.$http.post(url).then(function(request) {
                        this.group_stage = true;
                        this.getTeams();
                    });
                }
            },
            getTeams: function () {
                let url = base_url + 'api/teams/' + this.type;
                this.loading = true;
				this.$http.get(url).then(function(request) {
                    this.teams = [];
                    this.teams = this.teams.concat(request.body);
                    this.loading = false;
				});
            },
            generatePhase: function (type) {
                this.loading = true;
                this.new_champ = false;
                this.group_stage = (type == "group-stage") ? true : false;
                this.type = type;
                let url = base_url + 'api/games';
				this.$http.post(url, {type: type}).then(function(request) {
                    if (request.body.status) {
                        this.getTeams();
                    } else {
                        this.teams = [];
                        this.loading = false;
                    }
				});
            }
		}
	});

    app.getTeams();
</script>

<?php $this->load->view("footer"); ?>
