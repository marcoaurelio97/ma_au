<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Championship extends CI_Controller {

	public function __construct()
    {
		  parent::__construct();
    }

    public function index()
    {
      $this->load->model("model_api");
      $this->model_api->verifyDatabase();
      $this->load->view("championship");
    }
}