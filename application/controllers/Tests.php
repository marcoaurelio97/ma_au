<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tests extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
        $this->load->library("unit_test");
        $this->load->model("model_api");
    }

    public function index()
    {
        $this->model_api->generateChampionship();

        $this->qtyInitialTeams();
        $this->generateValidGame();
        $this->generateInvalidGame();
        $this->qtyRoundsWinner();
    }

    private function qtyInitialTeams()
    {
        $test_qty = count($this->model_api->getTeams());
        $test_expected_number = 80;
        $test_name = "Quantidade de times ao gerar um novo campeonato";
        echo $this->unit->run($test_qty, $test_expected_number, $test_name);
    }

    private function generateValidGame()
    {
        $test_qty = $this->model_api->generateGames("group-stage");
        $test_expected_number = [ "status" => true ];
        $test_name = "Processamento de uma etapa válida do jogo";
        echo $this->unit->run($test_qty, $test_expected_number, $test_name);
    }

    private function generateInvalidGame()
    {
        $test_qty = $this->model_api->generateGames("a");
        $test_expected_number = [ "status" => false ];
        $test_name = "Processamento de uma etapa inválida do jogo";
        echo $this->unit->run($test_qty, $test_expected_number, $test_name);
    }

    private function qtyRoundsWinner()
    {
        $match = $this->model_api->simulateGame("1", "2", "");

        $winner = $match["ga_winner"];

        if ($winner == "1") {
            $test_rounds = $match["ga_team1_rounds"];
        } else {
            $test_rounds = $match["ga_team2_rounds"];
        }

        $test_expected_number = 16;
        $test_name = "Quantidade de rounds do vencedor de um jogo";
        echo $this->unit->run($test_rounds, $test_expected_number, $test_name);
    }
}
