<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->model("model_api");
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: Content-Type");
    }

	public function championship()
	{
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$response = $this->model_api->generateChampionship();
			$this->sendData($response);
		}
	}

	public function games()
	{
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$data = json_decode(file_get_contents("php://input"));
			$this->sendData($this->model_api->generateGames($data->type));
		}
	}

	public function teams($type = false)
	{
		if ($_SERVER["REQUEST_METHOD"] == "GET") {
			$this->sendData($this->model_api->getTeamsPhase($type));
		}
	}

	private function sendData($arr)
	{
		header("Content-type: application/json; charset: utf8");
		echo json_encode($arr);
		die;
	}
}
